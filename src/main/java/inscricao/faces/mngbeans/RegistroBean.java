/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import utfpr.faces.support.PageBean;

/**
 *
 * @author fee
 */
@Named
@ApplicationScoped
public class RegistroBean extends PageBean{

    private ArrayList<Candidato> CandidatosList;
    
    public void setCandidatosList(Candidato c){
        if(this.CandidatosList == null)
            this.CandidatosList = new ArrayList<>();
        
        this.CandidatosList.add(c);
        System.out.println(c.getNome());
    }
    
    public ArrayList getCandidatosList() {
        return this.CandidatosList;
    }
    
    public int getSize(){
        return this.CandidatosList.size();
    }

}
